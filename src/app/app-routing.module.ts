import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { GettingpicsComponent } from './gettingpics/gettingpics.component';
import { IfelseComponent } from './ifelse/ifelse.component';
import { NgdestroytestComponent } from './ngdestroytest/ngdestroytest.component';
import { Ngdestroytest1Component } from './ngdestroytest1/ngdestroytest1.component';
import { Ngdestroytest2Component } from './ngdestroytest2/ngdestroytest2.component';
import { ParentComponent } from './parent/parent.component';
import { RformsComponent } from './rforms/rforms.component';

const routes: Routes = [
  {
    path:'ngdestroytest',
    component:NgdestroytestComponent
  },
  {
    path:'ngdestroytest1',
    component:Ngdestroytest1Component
  },
  {
    path:'ngdestroytest2',
    component:Ngdestroytest2Component
  },
  {
    path:'rforms',
    component:RformsComponent
  }
  ,
  {
    path:'comp1',
    component:Component1Component
  }
  ,
  {
    path:'comp2',
    component:Component2Component
  },
  {
    path:'ifelse',
    component:IfelseComponent
  },
  {
    path:'gettingpics',
    component:GettingpicsComponent
  },
  {
    path:'parent',
    component:ParentComponent
  },
  { path: 'lazyloading', loadChildren: () => import('./lazyloading/lazyloading.module').then(m => m.LazyloadingModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
