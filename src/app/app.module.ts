import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RformsComponent } from './rforms/rforms.component';
import {ReactiveFormsModule} from '@angular/forms';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { IfelseComponent } from './ifelse/ifelse.component';
import { GettingpicsComponent } from './gettingpics/gettingpics.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './parent/child/child.component';
import { NgdestroytestComponent } from './ngdestroytest/ngdestroytest.component';
import { Ngdestroytest1Component } from './ngdestroytest1/ngdestroytest1.component';
import { Ngdestroytest2Component } from './ngdestroytest2/ngdestroytest2.component';
@NgModule({
  declarations: [
    AppComponent,
    RformsComponent,
    Component1Component,
    Component2Component,
    IfelseComponent,
    GettingpicsComponent,
    ParentComponent,
    ChildComponent,
    NgdestroytestComponent,
    Ngdestroytest1Component,
    Ngdestroytest2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
