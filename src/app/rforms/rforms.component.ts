import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule,Validators} from '@angular/forms';
@Component({
  selector: 'app-rforms',
  templateUrl: './rforms.component.html',
  styleUrls: ['./rforms.component.scss']
})
export class RformsComponent implements OnInit {

  constructor() { }
  
  public myform=new FormGroup({
  'name':new FormControl('',[Validators.required]),
  'contact':new FormControl('',[Validators.required,Validators.pattern('^[0-9]{10}$')]),
  'email':new FormControl('',[Validators.required])
  });
  ngOnInit(): void {
  }

}
